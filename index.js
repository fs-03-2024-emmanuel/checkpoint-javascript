// Create a function that takes two numbers as arguments and returns their sum.

function addition(a, b) {
	return a + b
}

// Create a function which returns the number of true values there are in an array.

function countTrue(arr) {
	let summ = 0;
	for (let i of arr){
		if (i === true){ summ++};
	}
	return summ;
	console.log(summ)
}

// Fix the code in the code tab to pass this challenge (only syntax errors). Look at the examples below to get an idea of what the function should do.
function cubes(a) {
	return a ** 3
}

//Create a function that takes an array containing only numbers and return the first element.
function getFirstValue(arr) {
	return arr[0]
}

//Create a function that finds the maximum range of a triangle's third edge, where the side lengths are all integers.
function nextEdge(side1, side2) {
	return (side1 + side2) -1
	
}

//Create a function that takes a "base number" as an argument. This function should return another function which takes a new argument, and returns the sum of the "base number" and the new argument.
function makePlusFunction(baseNum) {
	return plusnum = num => {return num + baseNum}
}

//Write a function that takes a number and returns the perimeter of either a circle or a square. The input will be in the form (letter l, number num) where the letter will be either "s" for square, or "c" for circle, and the number will be the side of the square or the radius of the circle.
function perimeter(l, num){
	if( l==='s'){return num * 4}
	else{ return num * 6.28};
}

//Create a function that will return an integer number corresponding to the amount of digits in the given integer num.
function num_of_digits(num) {
	let convertNum = num.toString();
	let removeNeg = convertNum.replace("-","")
	let removDeci = removeNeg.replace(".","")
	return removDeci.length
}

//Given a number, n, return a function which adds n to the number passed to it.
function add(n) {
	return (n2) => {return n + n2}
}